#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(500, 500);
}

MyPainter::~MyPainter()
{
	//delete[] TH;
	//delete[] blok;
}

void MyPainter::ActionOpen()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), "", "image files (*.png *.jpg *.bmp)");
	if (!fileName.isEmpty())
		paintWidget.openImage(fileName);
}

void MyPainter::ActionSave()
{
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"), "untitled.png", tr("png Files (*.png)"));
	if (fileName.isEmpty()) {
		return;
	}
	else {
		paintWidget.saveImage(fileName);
	}
}

void MyPainter::EffectClear()
{
	paintWidget.clearImage();
	paintWidget.vymazBody();
	paintWidget.body.clear();
}

void MyPainter::ActionNew()
{
	paintWidget.newImage(800, 600);
}

void MyPainter::Kresli()
{
	int x1, x2, y1, y2;
	if (ui.comboBox->currentIndex() == 0) {
		paintWidget.DDA(ui.spinBox_2->value(), ui.spinBox_3->value(), ui.spinBox_4->value());
		paintWidget.naplnTH();
		paintWidget.ScanLine();
	}
}	