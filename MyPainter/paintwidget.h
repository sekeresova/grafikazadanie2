#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <cmath>
#include<qalgorithms.h>
#include <QVector>
#include <stdio.h>
#include <qpainter.h> 

using namespace std;

class PaintWidget : public QWidget
{
	Q_OBJECT

public:
	std::vector<QPoint> body;
	QImage image;
	PaintWidget(QWidget *parent = 0);
	bool openImage(const QString &fileName);
	bool newImage(int x, int y);
	bool saveImage(const QString &fileName);
	void setPenColor(const QColor &newColor);
	void setPenWidth(int newWidth);
	void vymazBody();
	void naplnTH();
	void DDA(int red, int green, int blue);
	void ScanLine();
	bool isModified() const { return modified; }
	QColor penColor() const { return myPenColor; }
	int penWidth() const { return myPenWidth; }

	public slots:
	void clearImage();

protected:
	void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
	void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
	void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
	void mouseDoubleClickEvent(QMouseEvent *event);

private:
	void drawLineTo(const QPoint &endPoint);
	void resizeImage(QImage *image, const QSize &newSize);

	int riadok;
	int stlpec;
	int pocetbodov = 0;
	double w;
	QVector<QVector<float>> TH;
	QColor color;
	bool modified;
	bool painting;
	int myPenWidth;
	QColor myPenColor;
	QPoint lastPoint;

};

#endif // PAINTWIDGET_H
